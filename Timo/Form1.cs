using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Timo
{

public class Win32 {
    [DllImport("user32.dll", CharSet=CharSet.Auto)]
    public static extern IntPtr MessageBox(int hWnd, String text, 
                 String caption, uint type);

    [DllImport("user32.dll", CharSet=CharSet.Auto)]
    public static extern bool FlashWindow(IntPtr hwnd, bool bInvert);
}


    public partial class Form1 : Form
    {
        private int interval    = 0;
        private int tjam        = 0;
        private int tmenit      = 0;
        private int tdetik      = 0;
        private int tmdetik     = 0;

        public Form1()
        {
            InitializeComponent();
            jam.Text = "0";
            menit.Text = "0";
            detik.Text = "0";
        }

        private void btStart_Click(object sender, EventArgs e)
        {
            if (!timer.Enabled)
            {
                interval = Int32.Parse(jam.Text) * 60 * 60 * 10 +
                    Int32.Parse(menit.Text) * 60 * 10 +
                    Int32.Parse(detik.Text) * 10;
                timer.Interval = 100;
                timer.Enabled = true;
                btStart.Text = "Stop";

                jam.Enabled = false;
                menit.Enabled = false;
                detik.Enabled = false;
            }
            else
            {
                jam.Enabled = true;
                menit.Enabled = true;
                detik.Enabled = true;
                timer.Enabled = false;
                btStart.Text = "Start";
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            //if (tjam == 0 && tmenit == 0 && tdetik == 0) timer.Enabled = false;
            if (interval-- <= 0)
            {
                jam.Enabled = true;
                menit.Enabled = true;
                detik.Enabled = true;
                timer.Enabled = false;
                btStart.Text = "Start";
                Win32.FlashWindow(this.Handle, true);
                //MessageBox.Show("The Time is Now!!!", "Yooo!");
                

            }
            else
            {
                tmdetik++;
                if (tmdetik == 10)
                {
                    tmdetik = 0;
                    tdetik++;
                    if (tdetik == 60)
                    {
                        tdetik = 0;
                        tmenit++;
                        if (tmenit == 60)
                        {
                            tmenit = 0;
                            tjam++;
                        }
                    }
                }
                if (tjam.ToString().Length == 1) time.Text = "0" + tjam.ToString();
                    else time.Text = tjam.ToString();
                if (tmenit.ToString().Length == 1) time.Text += ":" + "0" + tmenit.ToString();
                    else time.Text += ":" + tmenit.ToString();
                if (tdetik.ToString().Length == 1) time.Text += ":" + "0" + tdetik.ToString();
                    else time.Text += ":" + tdetik.ToString();
                time.Text += "`" + tmdetik + "0";
            }
        }
    }
}