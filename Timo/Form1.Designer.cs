namespace Timo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.time = new System.Windows.Forms.Label();
            this.btStart = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.timeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jam = new System.Windows.Forms.TextBox();
            this.menit = new System.Windows.Forms.TextBox();
            this.detik = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 10;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // time
            // 
            this.time.AutoSize = true;
            this.time.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.time.Location = new System.Drawing.Point(37, 45);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(218, 42);
            this.time.TabIndex = 0;
            this.time.Text = "00:00:00`00";
            // 
            // btStart
            // 
            this.btStart.Location = new System.Drawing.Point(205, 115);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(75, 23);
            this.btStart.TabIndex = 1;
            this.btStart.Text = "Start";
            this.btStart.UseVisualStyleBackColor = true;
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.timeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(292, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // timeToolStripMenuItem
            // 
            this.timeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.timeToolStripMenuItem.Name = "timeToolStripMenuItem";
            this.timeToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.timeToolStripMenuItem.Text = "Options";
            // 
            // jam
            // 
            this.jam.Location = new System.Drawing.Point(12, 115);
            this.jam.Name = "jam";
            this.jam.Size = new System.Drawing.Size(31, 21);
            this.jam.TabIndex = 3;
            this.jam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // menit
            // 
            this.menit.Location = new System.Drawing.Point(50, 115);
            this.menit.Name = "menit";
            this.menit.Size = new System.Drawing.Size(31, 21);
            this.menit.TabIndex = 4;
            this.menit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // detik
            // 
            this.detik.Location = new System.Drawing.Point(87, 115);
            this.detik.Name = "detik";
            this.detik.Size = new System.Drawing.Size(31, 21);
            this.detik.TabIndex = 5;
            this.detik.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 150);
            this.Controls.Add(this.detik);
            this.Controls.Add(this.menit);
            this.Controls.Add(this.jam);
            this.Controls.Add(this.btStart);
            this.Controls.Add(this.time);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Timo!";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.Button btStart;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem timeToolStripMenuItem;
        private System.Windows.Forms.TextBox jam;
        private System.Windows.Forms.TextBox menit;
        private System.Windows.Forms.TextBox detik;
    }
}

